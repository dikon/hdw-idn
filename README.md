# Identifikationsnummern für das Hallesche Druckwesen

Durch die Bestrebungen der letzten Jahrzehnte, eine retrospektive Nationalbibliographie für den deutschen Sprachraum zu schaffen, und nicht zuletzt dank dem als Digitalisatenkatalog konzipierten [VD18](http://uri.gbv.de/database/vd18) sind die Veröffentlichungen der halleschen Druckereien und Verlage zur Zeit der Aufklärung inzwischen weitreichend erschlossen und digital zugänglich.

Von diesen Quellen ausgehend wurden umfassend [Katalogisate zu Druckwerken hallescher Verlage und Druckereien](https://gitlab.informatik.uni-halle.de/dikon/hdw-pica) bereitgestellt, die bibliothekarischen Identifikationsnummern der einzelnen Datensätze extrahiert und hier schließlich getrennt nach Katalogen und Publikationsgattungen in separaten [JSON-Dateien](./data/) verzeichnet [\*].

---

[\*] Für Katalogisate des VD17 wurde auf die VD17-Nummer zurückgegriffen, für Katalogisate des VD18 auf die VD18-Nummer.
